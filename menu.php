<?php  include('server.php')?>
<?php 
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['Username']);
    header("location: login.php");
  }

?>


<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
        body{
            background-color:gainsboro;
            top: 0;
            }
        .content{
            background-color: aliceblue;
            width: 80%;
            height: auto;
            margin: auto;
            top: 0;
            padding: 50px;
            border-radius: 10px;
        }
        .menu li a{
            color: black;
            text-decoration: none;
            font-size: 30px;
        }
        .menu li{
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: gainsboro;
            height: 50px;

        }
        .menu a:hover{
            color: red;
            transition: .6s;
        }
        .menu ul{
            list-style-type: none;
            display: flex;
        }
        .menu ul li{
            width: 100vw;
        }

        .menu{
            letter-spacing: 3px;
        }
</style>
<body>
        <div class="menu" style="width: 75.1%;margin-left: 160px;top: 0px;bottom: 0px;">
        <ul>
            <li><a href="menu.php?logout='1'" style="color: red;text-decoration: none">LogOut</a></li>
            <li><a href="goods.php">Products</a></li>
            <li><a href="viewCart.php">Cart</a></li>
            <li><a href="login.php">Join us</a></li>
        </ul>
    </div>
</body>
</html>
