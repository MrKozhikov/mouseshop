<?php
session_start();
setcookie(session_name(), '',100);
unset($_SESSION['users']);
session_destroy();
$_SESSION = array();
header("Location: login.php");
?>