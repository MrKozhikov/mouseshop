<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Inconsolata&display=swap" rel="stylesheet">
  <script type="text/javascript">
    $(document).ready(function() {
          $("#click").click(function() {
          $("#1").fadeOut("fast", function() {
          });
      });
    });
  </script>
  
  <style>
    table
    {
      font-family: 'Inconsolata', monospace;
      border-collapse: collapse;
      width: auto;
      height: auto;
      font-size: 20px;

    }
    td, th
    {
      padding: 10px;
      border: 2px solid gainsboro;
    }
</style>
</head>
<body>
  <?php require 'menu.php'; ?>
  <div class="content">
    <?php
      require_once "connectionDB.php";

      $stmt = $pdo->query("SELECT * FROM goods_table");
    ?>
      <table><tr><th>Id</th><th>Name</th><th>Price/tg</th><th>Comment</th><th>Photo</th></tr>
      <?php
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
      {
        echo "<tr><td>";
        echo($row['goods_id']);
        echo "</td>";

        echo "<td>";
        echo($row['goods_name']);
        echo "</td>";
    
        echo "<td>";
        echo($row['goods_price']);
        echo "</td>";
    
        echo "<td>";
        echo($row['goods_comment']);
        echo "</td>";

    
        echo "<td>";
        ?>
        <img style="width: 140px;height: 120px;" src="<?= $row['goods_photo_path'] ?>" alt="image not found" width="65">
        <?php
        echo "</td></tr>";
      }
      ?>
    </table>

    <div style="margin-top: 30px;">
        <form action="goods.php" method = "post" style="margin-bottom: 10px;">
            <input type="text" name="mouse_name" placeholder="write the name of mouse">
            <input type="submit"  value="Delete" style="color: red;border-radius: 7px; " >
        </form>
        <?php
        if(isset($_POST['mouse_name'])){
            $mouse_name = $_POST['mouse_name'];
            $sql = "DELETE FROM goods_table WHERE goods_name = '$mouse_name'";
            
            mysqli_query($conn, $sql);
            echo '<script>alert("Chosen mouse was deleted from the database!");</script>';
            header("Refresh:1");
        }
        ?>

        <button style="border-radius: 7px; border-color: green"><a href="uploadGoods.php" style="text-decoration: none;color: black;">Deleted the wrong product, so <label style="color: red">create</label> it again</a></button>
        
  </div>
  <footer><center><a href="#"  id="click" ><img  id="1" src="https://www.internationalinnerwheel.org/assets/files/logos/THEME%202019%20LOGO%20SIDE.png" alt="" width="200" height="150"></a></center></footer>
  </div>
</body>
</html>



