<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta charset="utf-8">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Inconsolata&display=swap" rel="stylesheet">
  <script type="text/javascript">
    $(document).ready(function() {
          $("#click").click(function() {
          $("#1").fadeOut("fast", function() {
          });
      });
          $("button").click(function(){
            $("#p1").css("color", "red").slideUp(2000).slideDown(2000);
          });
    });
  </script>
  
  <style>
    table
    {
      font-family: 'Inconsolata', monospace;
      border-collapse: collapse;
      width: auto;
      height: auto;
      font-size: 20px;

    }
    td, th
    {
      padding: 10px;
      border: 2px solid gainsboro;
    }
    .content{
      width: auto;
    }

</style>
</head>
<body>
  <?php require 'menu.php'; ?>
  <div class="content">
    <?php
      require_once "connectionDB.php";
      $stmt = $pdo->query("SELECT * FROM goods_table ORDER BY goods_id ASC");
    ?>
      <table><tr><th>Id</th><th>Name</th><th>Price/tg</th><th>Comment</th><th>Photo</th><th>Quantity</th><th>Add</th></tr>
      <?php
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
      {
        echo "<form action='insertCart.php' method= 'POST'>";
        echo "<tr><td>";
        echo($row['goods_id']);
        echo "</td>";

        echo "<td>";
        echo($row['goods_name']);
        echo "</td>";
    
        echo "<td>";
        echo($row['goods_price']);
        echo "</td>";
    
        echo "<td>";
        echo($row['goods_comment']);
        echo "</td>";

    
        echo "<td>";
        ?>
        <img style="width: 140px;height: 120px;" src="<?= $row['goods_photo_path'] ?>" alt="image not found" width="65"></td>
        <td><input type="text" name="qty"></td>
        <input type="hidden" name="name" value="<?php echo $row["goods_name"] ?>">
        <input type="hidden" name="price" value="<?php echo $row["goods_price"] ?>">
        <td><input type="submit" name="addCart" value="Add Cart"></td>
        <?php
        echo "</tr>";
        echo "</form>";
      }
      ?>
    </table>

    <!-- jQuery METHOD OF CHAINING -->
        <p id="p1">jQuery is fun!!</p>
        <button>Click me</button>

        <?php
        //--Syntax of GENERATORS--
        function gen_one_to_three() {
            for ($i = 1; $i <= 3; $i++) {
                yield $i;
            }
        }

        $generator = gen_one_to_three();
        foreach ($generator as $value) {
            echo "$value\n";
        }
        ?>


  <footer><center><a href="#"  id="click" ><img  id="1" src="https://www.internationalinnerwheel.org/assets/files/logos/THEME%202019%20LOGO%20SIDE.png" alt="" width="200" height="150"></a></center></footer>
    <?php die(); ?>
  </div>
</body>
</html>




