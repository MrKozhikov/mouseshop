<?php
//--CONNECTION TO DATABASE--
require("connectionDB.php");
//--AUTENTIFICATION USING PDO METHODS
$anyvar = $pdo->query("SELECT*FROM users");
$boolean = true;
if(isset($_POST['signUp'])) {
	$username = $_POST['username'];
	while ($row = $anyvar->fetch(PDO::FETCH_ASSOC)) {
		if($_POST['username']==$row['Username']) {
			$boolean = false;
		}
		# code...
	}

	//--Checking on SAME USERNAME
	if($boolean) {
		$username = $_POST['username'];
		$boolean = $pdo->query("INSERT INTO users  VALUES('$username', '$email', '$password')");

		if($boolean) 
			$stmt = "<p style='text-align: center;color:green;'>New record has been created! </p>";
		      header('location: login.php');
	}
		else {
			$stmt = "<p style='text-align: center;color:red;'>Written username is already exist!</p>";
		    echo $stmt;
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Lora&display=swap" rel="stylesheet">
</head>
<style type="text/css">
	*{
		margin:0px;
		padding: 0px;
	}
	body{
		font-size: 1.1em;
		font-family: 'Lora',serif;
		background-color: gainsboro;

	}
	.header{
		font-size: 25px;
		width: 30%;
		margin:50px auto 0px ;
		color: black;
		background:#66ccff;
		text-align: center;
		border: 1px solid #B0C4DE;
		border-bottom: none;
		border-radius: 10px 10px 0px 0px;
		padding: 20px;
	}
	form{
		width: 30%;
		margin: 0px auto;
		padding: 20px;
		border:1px solid #B0C4DE;
		background:white;
		border-radius: 1px 0px 10px 10px; 
	}
	.input-group label {
		display: block;
		text-align: left;
		margin: 3px;
	}
	.input-group input {
		height: 30px;
		width: 93%;
		padding: 5px 10px;
		font-size: 16px;
		border-radius: 5px;
		border: 1px solid gray; 
	}
	.btn{
		padding: 10px;
		font-size: 15px;
		color: blue;
		background: #66ccff;
		border:none;
		border-radius: 5px; 
		margin-top: 5%; 
	}
</style>
<body>
    <div class="header">Register</div>
	<form method="post" action="register.php">
		<div class="input-group">
        	<label>Username</label>
		    <input type="text" name="username">	
		</div>
		<div class="input-group">
			<label>Email</label>
	        <input type="text" name="email">
		</div>
	    <div class="input-group">
	    	<label>Password</label>
	    	<input type="password" name="password">
	    </div>
        <div class="input-group" >
        	<button type="submit" class="btn" name="signUp">Login</button>
        </div>
	</form>
</body>
</html>