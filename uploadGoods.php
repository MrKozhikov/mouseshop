<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Lora&display=swap" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<?php require 'menu.php'; ?>

<div class="content">
  <form action="goods_creation.php" method="post" enctype="multipart/form-data">
    <div class="profileText">
      <input type="text" name="goods_id" placeholder="Goods id" requred><br><br>
      <input type="text" name="goods_name" placeholder="Goods name" requred><br><br>
      <input type="number" name="goods_price" placeholder="Goods price" requred><br><br>
      <input type="text" name="goods_comment" placeholder="Enter info about good" requred><br><br>
      <input type="file" name="image_to_upload"><br><br>
      <input class="sign-up-submit" value="Create" type="submit">
    </div>
  </form>
</div>
</body>
</html>
