<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lora&display=swap" rel="stylesheet">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>
    <style>

    *{
        margin:0px;
        padding: 0px;
    }
    body{
        font-size: 1.1em;
        font-family: 'Lora',serif;
        background-color:gainsboro;
        top: 0;
    }
    .header{
        font-size: 1.5em;
        width: 30%;
        margin:50px auto 0px;
        color: black;
        background:#66ccff;
        text-align: center;
        border: 1px solid #B0C4DE;
        border-bottom: none;
        border-radius: 10px 10px 0px 0px;
        padding: 20px;
    }
    form{
        width: 30%;
        margin: 0px auto;
        padding: 20px;
        border:1px solid #B0C4DE;
        background:white;
        border-radius: 1px 0px 10px 10px; 
    }
    .input-group label {
        display: block;
        text-align: left;
        margin: 3px;
    }
    .input-group input {
        height: 30px;
        width: 93%;
        padding: 5px 10px;
        font-size: 16px;
        border-radius: 5px;
        border: 1px solid gray; 
    }
    .btn{
        padding: 10px;
        font-size: 15px;
        color: blue;
        background: #66ccff;
        border:none;
        border-radius: 5px; 
        margin-top: 5%; 
    }


    </style>
    <body>
        <?php require 'server.php'; ?>
        <div class="header">Login</div>
        <form method="post" action="login.php">
            <?php include('errors.php'); ?>
            <div class="input-group">
                <label>Username</label>
                <input type="text" name="username" required> 
            </div>
            <div class="input-group">
                <label>Password</label>
                <input type="password" name="password" required>
            </div>
            <div class="input-group" >
                <button type="submit" class="btn" name="signIn">Login as an User</button>
                <button type="submit" class="btn" name="signInAdmin" style="margin-left: 110px">Login as an Admin</button>
            <p style="margin-top: 10px;">
                Not yet a member? <a href="register.php" style="text-decoration: none;">Sign up</a>
            </p>
            </div>
        </form>
    </body>
</html>



