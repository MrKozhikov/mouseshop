<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Lora&display=swap" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <style>
table
{
  border-collapse: collapse;
}
td, th
{
  padding: 10px;
  border: 2px solid darkgray;
}
</style>
</head>
<body>
  <?php require 'menu.php'; ?>

  <div class="content">
    <?php
      require_once "connectionDB.php";
      require_once "photo_check.php";
      $goods_id = "";
      $goods_name = "";
      $goods_price = "";
      $goods_comment = "";
      $goods_photo_path = $target_file;
      if
      (
         isset($_POST["goods_id"]) && ($_POST["goods_name"]) && isset($_POST["goods_price"])
         && isset($_POST["goods_comment"])
      )
      {
        $goods_id = $_POST["goods_id"];
        $goods_name = $_POST["goods_name"];
        $goods_price = $_POST["goods_price"];
        $goods_comment = $_POST["goods_comment"];
      }
      $insert_sql = "INSERT INTO goods_table
      (
        goods_id,
        goods_name,
        goods_price,
        goods_comment,
        goods_photo_path
      )
      VALUES
      (
        :goods_id,
        :goods_name,
        :goods_price,
        :goods_comment,
        :goods_photo_path
      )";
      echo $insert_sql;
      $stmt = $pdo->prepare($insert_sql);
      $stmt->execute
        (
          array
          (
            ':goods_id' => $_POST['goods_id'],
            ':goods_name' => $_POST['goods_name'],
            ':goods_price' => $_POST['goods_price'],
            ':goods_comment' => $_POST['goods_comment'],
            ':goods_photo_path' => $goods_photo_path
          )
        );
    ?>
  </div>
</body>
</html>



