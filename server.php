<?php
session_start();

$username = "";
$email    = "";
$errors = array(); 


$db = mysqli_connect('localhost', 'root', '', 'online_shop');
// LOGIN USER
if (isset($_POST['signIn'])) {
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $password = mysqli_real_escape_string($db, $_POST['password']);

  if (empty($username)) {
    array_push($errors, "username is required");
  }
  if (empty($password)) {
    array_push($errors, "Password is required");
  }

  if (count($errors) == 0) {
    $password = $password;
    $query = "SELECT * FROM users WHERE Username='$username' AND Password='$password'";
    $results = mysqli_query($db, $query);
    if (mysqli_num_rows($results) == 1) {
      header('location: user_dashboard.php');
    }else {
      array_push($errors, "Wrong username/Password combination");
    }
  }
}

if (isset($_POST['signInAdmin'])) {
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $password = mysqli_real_escape_string($db, $_POST['password']);

  if (empty($username)) {
    array_push($errors, "username is required");
  }
  if (empty($password)) {
    array_push($errors, "password is required");
  }

  if (count($errors) == 0) {
    $password = $password;
    $query = "SELECT * FROM admin WHERE Username='$username' AND Password='$password'";
    $results = mysqli_query($db, $query);
    if (mysqli_num_rows($results) == 1) {
      header('location: goods.php');
    }else {
      array_push($errors, "Wrong username/Password combination");
    }
  }
}
?>