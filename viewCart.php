<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta charset="utf-8">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Inconsolata&display=swap" rel="stylesheet">
  <script type="text/javascript">
    $(document).ready(function() {
          $("#click").click(function() {
          $("#1").fadeOut("fast", function() {
          });
      });
    });
  </script>
  
  <style>
    table
    {
      font-family: 'Inconsolata', monospace;
      border-collapse: collapse;
      width: auto;
      height: auto;
      font-size: 20px;

    }
    td, th
    {
      padding: 10px;
      border: 2px solid gainsboro;
    }
    .content{
      width: auto;
    }

</style>
</head>
<body>
  <?php require 'menu.php'; ?>
  <div class="content">
    <table class="table">
      <thead>
        <tr>
          <th>N</th>
          <th>Name</th>
          <th>Price</th>
          <th>Quantity</th>
          <th>Total Price</th>
          <th>Update</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $sum = 0;
        $i = 1;
        foreach ($_SESSION as $products) {
        $j = 0;
        $k = 0;
          echo "<tr>";
          echo "<td>".($i++)."</td>";
          echo "<form action='editCart.php' method='POST'>";
          foreach ($products as $key => $value) {
            if($key  == 2) {
              echo "<td><input type='text' name='name$key' class = 'form-control' value='".$value."'></td>";
              $j = $value;
            } elseif ($key == 1) {
              echo "<input type='hidden' name='name$key' value = '".$value."'>";
              echo "<td>".$value."</td>";
              $k = $value;
              # code...
            } elseif ($key == 0) {
              echo "<input type='hidden' name='name$key' value='".$value."'>";
              echo "<td>".$value."</td>";
              # code...
            }
            # code...
          }
          $sum = ($j * $k);
          echo "<td>".($sum)."</td>";
          echo "<td><input type='submit' name='event' value='Update' class='btn btn-warning'></td>";
          echo "<td><input type='submit' name='event' value='Delete' class='btn btn-danger'></td>";
          echo "</form>";
          echo "<tr>";
           # code...
         } 
        ?>
      </tbody>
    </table>
    
    <?php
    $num = (isset($_COOKIE["num"])) ? $_COOKIE["num"] : 0;
    $num++;
    setcookie("num", $num, time() + 4);
    echo "The user updated the page $num times!";
    ?>
    
    <br><br><button style="border-radius: 7px; "><a href="user_dashboard.php" style="text-decoration: none;color: black;">Continue shopping</a></button>
  <footer><center><a href="#"  id="click" ><img  id="1" src="https://www.internationalinnerwheel.org/assets/files/logos/THEME%202019%20LOGO%20SIDE.png" alt="" width="200" height="150"></a></center></footer>
  </div>
</body>
</html>




